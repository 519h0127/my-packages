import 'package:flutter_test/flutter_test.dart';
import 'package:string_util/string_util.dart';


void main() {
  test('Bỏ dấu tiếng việt', () {
    expect(stringFormater("Nguyễn Chí  Nhân"),"Nguyen Chi Nhan");
    expect(stringFormater("Đặng Ngọc Quỳnh Như"),"Dang Ngoc Quynh Nhu");
    expect(stringFormater("Võ Nguyễn   Duy Anh"),"Vo Nguyen Duy Anh");
    expect(stringFormater("Vy   Tiểu Mẫn"),"Vy Tieu Man");
    expect(stringFormater("Tô Minh    Tuấn"),"To Minh Tuan");
    expect(stringFormater("Ngô Đình    Tâm"),"Ngo Dinh Tam");
    expect(stringFormater("Nguyễn Hoàng Anh Khoi"),"Nguyen Hoang Anh Khoi");
  });
  test('Trả về số từ trong câu',(){
    expect(stringCounter("Nguyễn Chí  Nhân"), 3);
    expect(stringCounter("Đặng Ngọc Quỳnh Như"), 4);
    expect(stringCounter("Võ Nguyễn   Duy Anh"), 4);
    expect(stringCounter("Vy   Tiểu Mẫn"), 3);
    expect(stringCounter("Tô Minh    Tuấn Anh"), 4);
    expect(stringCounter("Ngô Đình    Tâm"), 3);
    expect(stringCounter("Nguyễn Hoàng Anh Khoi"), 4);
  });
}
