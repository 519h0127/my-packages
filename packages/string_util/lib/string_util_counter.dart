part of string_util;

int stringCounter(String str){
  return str.replaceAll(new RegExp(r"\s+"), " ").split(' ').length;
}