import 'package:flutter_test/flutter_test.dart';
import 'package:bignumber_util/bignumber_util.dart';


void main() {
  test('Cộng hai số lớn theo thứ tự', () {
    expect(BigNumberAddtion("899994273284999999998999999992498034289324","234859234879342897893427893274"),"899994273285234859233879342890391462182598");
    expect(BigNumberAddtion("8991123123123", "2348592348793428978934123"),"2348592348802420102057246");
    expect(BigNumberAddtion("1234", "892"),"2126");
  });

}
